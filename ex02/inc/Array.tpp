
#if !defined(ARRAY_TPP)
#define ARRAY_TPP

#include "Array.hpp"

//Constructors and destructor of Array class

template <class T>
Array<T>::Array(void)
	: _len(0)
{
	std::cout<<"Calling Array default constructor"<<std::endl;
}

template<class T>
Array<T>::Array(unsigned int len)
	:_len(len)
{
	std::cout<<"Calling Array constructor with 1 parameter <unsigned int>"<<std::endl;
	arr = new T[this->_len];
}

template<class T>
Array<T>::Array(const Array<T>& array_copy)
{
	std::cout<<"Calling Array copy constructor"<<std::endl;
	this->_len = 0;
	*this = array_copy;
}

template <class T>
Array<T>::~Array()
{
	std::cout<<"Calling Array destructor"<<std::endl;
	if(this->_len > 0)
		delete [] this->arr;
}

//Operators of Array class

template <class T>
Array<T>& Array<T>::operator= (const Array<T>& array_copy)
{
	if(this != &array_copy)
	{
		if(this->_len > 0)
			delete [] this->arr;
		
		this->_len = array_copy.size();

		this->arr = new T[array_copy.size()];
		for(size_t i = 0; i < array_copy.size(); i++)
		{
			this->arr[i] = array_copy.arr[i];
		}
	}

	return *this;
}

template <class T>
T& Array<T>::operator[] (int index) 
{
	if(index < 0 || index >= static_cast<int>(this->_len))
		throw Array<T>::OutOfRangeException("Index not valid");
	return (this->arr[index]);
}

//Methods of Array class
template <class T>
size_t Array<T>::size (void) const
{
	return (this->_len); 
}

#endif

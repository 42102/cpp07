
#if !defined(ARRAY_HPP)
#define ARRAY_HPP

#include <iostream>

template <class T>
class Array
{
	private:
		size_t _len;
		T *arr;

	public:
		//Internal class to throw a exception	
		class OutOfRangeException: public std::exception
		{
			public:
				OutOfRangeException(std::string str)
					: msg("OutOfRangeException: " + str)
				{}

				~OutOfRangeException() throw()
				{}

				const char *what() const throw()
				{
					return(this->msg).c_str();
				}

			private:
				std::string msg;
		};

		//Constructors and destructor
		Array(void);
		Array(unsigned int len);
		Array(const Array& array_copy);
		~Array();
		
		//Operators overloading	
		Array& operator= (const Array& array_copy);
		T& operator[] (int index ); 
		
		//Methods
		size_t size (void) const;
};

#include "Array.tpp"

#endif

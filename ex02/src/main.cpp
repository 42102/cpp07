
#include <iostream>
#include "../inc/Array.hpp"

int main(void)
{
	Array<float> a;
	Array<double> b(9);
	Array<double> c(29);
	Array<std::string> d(5);
	Array<std::string> e(d);
	c = b;

	std::cout<<"size of a: "<<a.size()<<std::endl;
	std::cout<<"size of b: "<<b.size()<<std::endl;
	std::cout<<"size of c: "<<c.size()<<std::endl;

	try
	{	
		c[3] = 223.123;
		std::cout<<"c[3]: "<<c[3]<<std::endl;
		
		c[8] = 98596.3;
		std::cout<<"c[8]: "<<c[8]<<std::endl;
	}
	catch(std::exception &e)
	{
		std::cout<<"error: "<<e.what()<<std::endl;
	}
	return (0);
}

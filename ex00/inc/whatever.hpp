
#if !defined(WHATEVER_HPP)
#define WHATEVER_HPP

#include <iostream>

template <typename A>
void swap(A& a, A& b)
{
	A aux;
	aux = a;
	a = b;
	b = aux;
}

template <typename B>
B min(const B& a, const B& b)
{
	if(a < b)
		return a;
	else
		return b;
}

template <typename C>
C max(const C& value1, const C& value2)
{
	if(value1 > value2)
		return value1;
	else 
		return value2;
}
#endif

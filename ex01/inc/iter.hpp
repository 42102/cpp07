
#if !defined(ITER_HPP)
#define ITER_HPP

template <typename Type_Array>
void iter(Type_Array *array, size_t len, void (*func_callback)(const Type_Array&))
{
	for(size_t i = 0; i < len; i++)
	{
		func_callback(array[i]);
	}
}

#endif
